# Tux GRUB theme
Just a theme of GRUB.
Supported languages: Chinese, English, French, German, Italian, Norwegian, Portuguese, Russian, Spanish, Catalan, Ukrainian.

### How to install
Download install script:
1- wget -P /tmp https://gitlab.com/escanyapobres/grub-theme-tux/raw/master/install.sh
2- Review install script at /tmp/install.sh
3- Run it: bash /tmp/install.sh